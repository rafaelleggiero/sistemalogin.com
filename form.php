<?php
include 'includes/header.php' ;
include 'includes/logica-usuario.php';
include 'banco-categoria.php';

$categorias = listaCategorias($pdo);
error_reporting(E_ALL ^ E_NOTICE);

session_start();
verificaUsuario();
?>
    <body>
    <section>
        <div class="container">
            <h1 class="wow fadeInLeft"> Adcionar produto </h1> 
            <form action="adciona-produto.php" method="post">
                <table class="table">
                    <tr>
                        <td>Nome:</td>
                        <td><input class="form-control" type="text" name="nome" id="nome"></td>
                    </tr>    
                    <tr>    
                        <td>Preço:</td>
                        <td><input class="form-control" type="number" name="preco" id="preco"></td>
                    </tr>
                     <tr>
                        <td>Descrição</td>
                        <td><textarea name="descricao" class="form-control"></textarea></td>
                    </tr> 
                    <tr>
                        <td></td>
                        <td><input type="checkbox" name="usado" value="true"> Usado</td>
                    </tr>
                    <tr>
                        <td>Categoria</td>
                        <td>
                            <select name="categoria_id">
                            <?php foreach($categorias as $categoria) : ?>
                            <option value="<?=$categoria['id']?>"><?=$categoria['nome']?></option>
                            <?php endforeach ?>
                            </select>
                        </td>
                    </tr>   
                    <tr>    
                        <td><button class="btn btn-primary" type="submit" value="Enviar">Enviar</button></td>
                        <td></td>
                    </tr>
                </table>
            </form>
        </div>
    </section>
  <?php include 'includes/footer.php' ?>  