<?php

function listaCategorias($pdo) {
    $categorias = array();
    $sql = $pdo->query("SELECT * FROM categorias");
    $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    foreach($result as $item){
        array_push($categorias,$item);
    }
    return $categorias;
}
