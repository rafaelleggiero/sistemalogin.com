<?php include 'includes/header.php' ?>
<?php include 'includes/pdo.php' ?>
<?php include 'banco-produto.php' ?>
 <?php include 'includes/logica-usuario.php'?> 
<?php
error_reporting(E_ALL ^ E_NOTICE);

session_start();
verificaUsuario();

?>
<?php if(array_key_exists("removido", $_GET) && $_GET['removido']=='true') { ?>
<p class="alert-success">Produto apagado com sucesso.</p>
<?php } ?>
<?php if(array_key_exists("adcionado", $_GET) && $_GET['adcionado']=='true') { ?>
<p class="alert-success">Produto Adcionado com sucesso.</p>
<?php } ?>
<?php if(array_key_exists("alterado", $_GET) && $_GET['alterado']=='true') { ?>
<p class="alert-success">Produto Alterado com sucesso.</p>
<?php } ?>
<div class="container">
  <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>Nome</th>
        <th>Preço</th>
        <th>Descrição</th>
        <th>Categoria ID</th>
        <th>Ações</th>
        <th>Ações</th>
      </tr>
    </thead>
    <tbody>
      <?php
      
        $produtos = listaProdutos($pdo);
        foreach($produtos as $item){
      ?>
        <tr>
          <td><?=  $item['Nome'] ?></td>
          <td><?= $item['Preco'] ?></td>
          <td><?= $item['Descricao'] ?></td>
          <td><?= $item['categoria_nome'] ?></td>
          <td><a class="btn btn-primary" href="produto-altera-formulario.php?id=<?=$item['Id']?>">alterar</a>
          <td>
          <form action="remove-produto.php" method="post">
                <input type="hidden" name="id" value="<?=$item['Id']?>" />
                <button class="btn btn-danger">remover</button>
            </form>
          </td>
        </tr>
        <?php 
      }     
      ?>
    </tbody>
  </table>
</div>
  <?php include 'includes/footer.php' ?>  
