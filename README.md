# LEIA ME #

### Para rodar o projeto: ###


* Vá para a pasta onde vai rodar o projeto usando o cmd
* Execute: git clone https://usuario@bitbucket.org/pasta/projeto.git 
* (Trocar pelo url do seu usuario)
* Digite [git pull] para pegar as ultimas atualizações no código fonte
* Verifique se está apontando para o banco correto

### Como subir o projeto ? ###

* Após fazer suas alterações e finalizar os ajustes, digite: gir status para verificar o git
* Digite: "git add ." para adicionar as novas alterações
* Digite: [git commit -m "Sua mensagem"] para fazer o commit
* Digite: [git push] para subir as alterações

### Desenvolvido por ###

* Rafael Leggiero
