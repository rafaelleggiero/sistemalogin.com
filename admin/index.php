<?php include_once $_SERVER['DOCUMENT_ROOT']."/includes/header.php" ?>
<?php include_once $_SERVER['DOCUMENT_ROOT']."/includes/pdo.php"?> 
<?php include_once $_SERVER['DOCUMENT_ROOT']."/includes/logica-usuario.php"?> 
<?php
error_reporting(E_ALL ^ E_NOTICE);

session_start();
verificaUsuario();
?>
    <section class="">
        <div class="container">
			 <h2>Bem-vindo</h2>
                <?php if(usuarioEstaLogado()) {?>
                <p class="text-success">Você está logado como <?= usuarioLogado() ?>.
                <a href="../login/logout.php">Deslogar</a></p>
            <?php } ?>
        </div>
    </section>
<?php include 'includes/footer.php' ?>  
