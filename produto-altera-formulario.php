<?php
include 'includes/header.php' ;
include 'includes/logica-usuario.php';
include 'banco-categoria.php';
include 'banco-produto.php';

$id = $_GET['id'];

$categorias = listaCategorias($pdo);
$produtos = buscaProduto($pdo,$id);

session_start();
verificaUsuario();
?>
    <body>
    <section>
        <div class="container">
            <h1 class="wow fadeInLeft"> Alterando produto</h1> 
            <form action="altera-produto.php" method="post">
                <table class="table">
                <?php foreach($produtos as $produto) : ?>
                <input type="hidden" name="id" value="<?=$produto['Id']?>" />
           
                    <tr>
                        <td>Nome:</td>
                        <td><input class="form-control" type="text" name="nome" id="nome" value="<?=$produto['Nome']?>"></td>
                    </tr>    
                    <tr>    
                        <td>Preço:</td>
                        <td><input class="form-control" type="number" name="preco" id="preco" value="<?=$produto['Preco']?>"></td>
                    </tr>
                     <tr>
                        <td>Descrição</td>
                        <td><textarea name="descricao" class="form-control"><?=$produto['Descricao']?></textarea></td>
                    </tr> 
                    <tr>
                        <td></td>
                        <?php
                            $usado = $produto['usado'] ? "checked='checked'" : "";
                        ?>
                        <td><input type="checkbox" name="usado" value="true" <?=$usado?>> Usado</td>
                    </tr>
                    <?php endforeach ?>
                    <tr>
                        <td>Categoria</td>
                        <td>
                            <select name="categoria_id" class="form-control">
                            <?php foreach($categorias as $categoria) :
                                
                                $essaEhACategoria = $produto['categoria_id'] == $categoria['id'];
                                $selecao = $essaEhACategoria ? "selected='selected'" : "";
                                ?>
                            
                            <option value="<?=$categoria['id']?>" <?=$selecao?>>
                                    <?=$categoria['nome']?>
                            </option>
                            <?php endforeach ?>
                            </select>
                        </td>
                    </tr>   
                    <tr>    
                        <td><button class="btn btn-primary" type="submit">Alterar</button></td>
                        <td></td>
                    </tr>
                </table>
            </form>
        </div>
    </section>
  <?php include 'includes/footer.php' ?>  